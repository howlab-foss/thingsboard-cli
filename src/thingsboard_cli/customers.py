
from tb_rest_client.rest_client_ce import RestClientCE # type: ignore
from tb_rest_client.models.models_ce import Customer, CustomerId  # type: ignore
from tb_rest_client.rest import ApiException # type: ignore

import json

from typing import Optional
import typer

from thingsboard_cli.cli_context import CliContext

app = typer.Typer()

@app.command()
def create(ctx: typer.Context, title: str):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    customer: Customer = Customer(title=title)
    try:
        customer = rest_client.save_customer(customer)
        typer.echo(customer)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

@app.command()
def delete(ctx: typer.Context, customer_id: str):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    customer_id: CustomerId = CustomerId('CUSTOMER', customer_id)
    try:
        rest_client.delete_customer(customer_id)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")