from tb_rest_client.rest_client_ce import RestClientCE  # type: ignore
from tb_rest_client.models.models_ce import EntityId, AssetSearchQuery, RelationsSearchParameters  # type: ignore
from tb_rest_client.rest import ApiException  # type: ignore

import json

from typing import List, Optional
import typer

from thingsboard_cli.cli_context import CliContext

app = typer.Typer()


@app.command()
def assets(ctx: typer.Context, root_id: str, root_type: str, relation_type: str, direction: str, types: List[str],
           relation_type_group: str = typer.Option('COMMON'),
           max_level: int = typer.Option(1),
           fetch_last_level_only: bool = typer.Option(False)):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client

    asset_search_query: AssetSearchQuery = AssetSearchQuery(
        parameters=RelationsSearchParameters(
            # entity_id=asset.id,
            root_id=root_id,
            root_type=root_type,
            direction=direction,
            relation_type_group=relation_type_group,
            max_level=max_level,
            fetch_last_level_only=fetch_last_level_only),
        relation_type=relation_type,
        asset_types=types
    )

    try:
        result = rest_client.find_by_query(asset_search_query)
        typer.echo(result)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")
