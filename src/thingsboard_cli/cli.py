"""Send greetings."""

import string
import configparser

from dotenv import find_dotenv, load_dotenv

from tb_rest_client.rest_client_ce import RestClientCE, User # type: ignore
from tb_rest_client.rest import ApiException # type: ignore
import logging
from thingsboard_cli.cli_context import CliContext


import thingsboard_cli.devices as devices
import thingsboard_cli.assets as assets
import thingsboard_cli.entity_views as entity_views
import thingsboard_cli.relations as relations
import thingsboard_cli.find as find
import thingsboard_cli.customers as customers
import thingsboard_cli.users as users

import json

import typer

app = typer.Typer()
app.add_typer(devices.app, name="devices")
app.add_typer(assets.app, name="assets")
app.add_typer(entity_views.app, name="entity_views")
app.add_typer(relations.app, name="relations")
app.add_typer(find.app, name="find")
app.add_typer(customers.app, name="customers")
app.add_typer(users.app, name="users")


@app.callback(invoke_without_command=True)
def main(ctx: typer.Context,
        base_url: str = typer.Option("localhost:8080", envvar="THINGSBOARD_URL"),
        username: str = typer.Option("sysdamin@thingsboard.org", envvar="THINGSBOARD_USERNAME"),
        password: str = typer.Option(..., prompt=True, hide_input=True, envvar="THINGSBOARD_PASSWORD"),
        customer: str = typer.Option(None)) -> None:

    cli_context = CliContext()

    # Init RestClient
    rest_client: RestClientCE = ctx.with_resource(RestClientCE(base_url))
    cli_context.rest_client = rest_client
    try:
        rest_client.login(username=username,password=password)
    except Exception as e:
        typer.echo(e,err=True)
        raise typer.Exit(code=1)

    try:
        user: User = rest_client.get_user()
    except Exception as e:
        typer.echo(e,err=True)
        raise typer.Exit(code=1)
    cli_context.user = user

    # Ask for customer if provided
    if customer is not None:
        if user.authority == 'CUSTOMER_USER':
            typer.echo('Warning! Customer param with a customer user will not take effect!', err=True, color=True)
        else:
            try:
                customer = rest_client.get_tenant_customer(customer)
                cli_context.customer = customer
            except Exception as e:                         
                typer.echo(e,err=True)
                raise typer.Exit(code=1)

    if user.authority == 'CUSTOMER_USER':
        try:
            customer = rest_client.get_customer_by_id(user.customer_id)
        except Exception as e:                
            typer.echo(e,err=True)
            raise typer.Exit(code=1)
        cli_context.customer = customer


    # Save in context
    ctx.obj = cli_context

    # if ctx.invoked_subcommand is None:
    #     typer.echo("Initializing database")

def run() -> None:
    """Run commands."""
    load_dotenv(find_dotenv(usecwd=True))    
    app()