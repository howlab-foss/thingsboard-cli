from datetime import datetime
from pathlib import Path
from tb_rest_client.rest_client_ce import RestClientCE # type: ignore
from tb_rest_client.models.models_ce import EntityId  # type: ignore
from tb_rest_client.rest import ApiException # type: ignore
from tabulate import tabulate
import pandas as pd
import sys
import os

import json

from thingsboard_cli.cli_context import CliContext

from typing import List, Optional
import typer

app = typer.Typer()

from enum import Enum



@app.command()
def public(ctx: typer.Context, unassign: bool = typer.Option(False, "--unassign")):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:

        if cli_context.target_entity.id.entity_type == 'ASSET':
            if unassign:
                rest_client.unassign_asset_from_customer(cli_context.target_entity.id)
            else:
                rest_client.assign_asset_to_public_customer(cli_context.target_entity.id)
        elif cli_context.target_entity.id.entity_type == 'DEVICE':
            if unassign:
                rest_client.unassign_device_from_customer(cli_context.target_entity.id)
            else:
                rest_client.assign_device_to_public_customer(cli_context.target_entity.id)

    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")