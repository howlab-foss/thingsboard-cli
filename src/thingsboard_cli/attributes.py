from tb_rest_client.rest_client_ce import RestClientCE # type: ignore
from tb_rest_client.models.models_ce import EntityId  # type: ignore
from tb_rest_client.rest import ApiException # type: ignore

import json

from thingsboard_cli.cli_context import CliContext

from typing import List, Optional
import typer

app = typer.Typer()

from enum import Enum

class AttributeScope(str, Enum):
    def __str__(self):
        return str(self.value)

    SERVER_SCOPE = "SERVER_SCOPE"
    SHARED_SCOPE = "SHARED_SCOPE"
    CLIENT_SCOPE = "CLIENT_SCOPE"

@app.command()
def set(ctx: typer.Context, data: str, scope: AttributeScope = typer.Option(AttributeScope.SERVER_SCOPE)):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        rest_client.save_entity_attributes_v1(entity_type=cli_context.target_entity.id.entity_type,entity_id=cli_context.target_entity.id.id,scope=scope,body=json.loads(data))
    except ApiException as e:
        typer.echo(f"{e.status}: {e.body}")

@app.command()
def delete(ctx: typer.Context, keys: str, scope: AttributeScope = typer.Option(AttributeScope.SERVER_SCOPE)):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        rest_client.delete_entity_attributes1(entity_id=EntityId(entity_type=cli_context.target_entity.id.entity_type,id=cli_context.target_entity.id.entity_type.id),scope=scope,keys=keys)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")