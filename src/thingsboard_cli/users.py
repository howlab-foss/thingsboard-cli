
from cgitb import text
from xmlrpc.client import Boolean
from tb_rest_client.rest_client_ce import RestClientCE # type: ignore
from tb_rest_client.models.models_ce import User, UserId  # type: ignore
from tb_rest_client.rest import ApiException # type: ignore

from thingsboard_cli.cli_context import CliContext

import json

from typing import Optional, List
import typer

from enum import Enum

class ActivationMethod(str, Enum):
    SEND_MAIL = "SEND_MAIL"
    DISPLAY_LINK = "DISPLAY_LINK"

app = typer.Typer()

@app.command()
def list(ctx: typer.Context, page_size: int = typer.Option(100),
         page: int = typer.Option(0), type: str = typer.Option(None),
         text_search: str = typer.Option(None),
         sort_property: str = typer.Option(None), sort_order: str = typer.Option(None)):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        if cli_context.customer is not None:
            device_list = rest_client.get_customer_device_infos(
                customer_id=cli_context.customer.id,
                page=page, page_size=page_size, type=type,
                text_search=text_search, sort_property=sort_property,
                sort_order=sort_order)
        elif cli_context.is_tenant_admin():
            device_list = rest_client.get_tenant_device_infos(
                page=page, page_size=page_size, type=type,
                text_search=text_search, sort_property=sort_property,
                sort_order=sort_order)

        typer.echo(device_list)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")


@app.command()
def create(ctx: typer.Context, email: str, first_name: str = typer.Option(None), last_name: str = typer.Option(None),
            description: str = typer.Option(None),
            default_dashboard_fullscreen: bool = typer.Option(False, "--default-dashboard-fullscreen"),
            default_dashboard_id: str = typer.Option(None),
            home_dashboard_hide_toolbar: str = typer.Option(False, "--home-dashboard-hide-toolbar")):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client

    additional_info = {
        "description": description,
        "default_dashboard_fullscreen": default_dashboard_fullscreen,
        "default_dashboard_id": default_dashboard_id,
        "home_dashboard_hide_toolbar": home_dashboard_hide_toolbar
    }

    user: User = User(email=email, first_name=first_name, last_name=last_name, additional_info=additional_info,authority="TENANT_ADMIN")

    if cli_context.customer is not None:
        user.customer_id = cli_context.customer.id
        user.authority = "CUSTOMER_USER"
    else:
        user.authority = "TENANT_USER"

    try:
        user: User = rest_client.save_user(user)
        typer.echo(user)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

@app.command()
def delete(ctx: typer.Context, user_id: str):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client

    user_id: UserId = UserId('USER', user_id)
    try:
        rest_client.delete_user(user_id)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")
