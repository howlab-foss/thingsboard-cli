from datetime import datetime
from pathlib import Path
from tb_rest_client.rest_client_ce import RestClientCE # type: ignore
from tb_rest_client.models.models_ce import EntityId  # type: ignore
from tb_rest_client.rest import ApiException # type: ignore
from tabulate import tabulate
import pandas as pd
import numpy as np
import sys
import os

import json

from thingsboard_cli.cli_context import CliContext

from typing import List, Optional
import typer

app = typer.Typer()

from enum import Enum

@app.command()
def keys(ctx: typer.Context):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        keys = rest_client.get_timeseries_keys_v1(entity_type=cli_context.target_entity.id.entity_type, entity_id=cli_context.target_entity.id.id)
        typer.echo(keys)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

@app.command()
def latest(
        ctx: typer.Context, 
        keys: str = typer.Option(None), 
        use_strict_data_types: bool = typer.Option(False, '--use-strict-data-types'),
        csv: bool = typer.Option(False, '--csv'),
        table: bool = typer.Option(False, '--table'),
        format_output_date: bool = typer.Option(False, '--format-output-date')
        ):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        latest = rest_client.get_latest_timeseries(entity_type=cli_context.target_entity.id.entity_type, entity_id=cli_context.target_entity.id.id,keys=keys,use_strict_data_types=use_strict_data_types)
        if csv or table:
            df: pd.DataFrame = pd.DataFrame()

            for key, records in  latest.items():
                series: pd.Series = pd.Series({r['ts']: r['value'] for r in records}, name=key)
                df = df.join(series, how='outer')

            df.index.name = 'ts'
            if format_output_date:
                df.index = pd.to_datetime(df.index, unit='ms')
            
            if csv:
                df.to_csv(sys.stdout)
            else:
                typer.echo(tabulate(df, headers=df.columns))
            
        else:
            typer.echo(latest)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

@app.command()
def get(
        ctx: typer.Context,
        keys: str = typer.Option(None),
        start_ts: datetime = typer.Option(..., formats=["%Y-%m-%d", "%Y-%m-%dT%H:%M:%S", "%Y-%m-%d %H:%M:%S", "%Y-%m-%dT%H:%M:%S.%Z", "%Y-%m-%d %H:%M:%S.%Z", "%m/%d/%Y"]),
        end_ts: datetime = typer.Option(..., formats=["%Y-%m-%d", "%Y-%m-%dT%H:%M:%S", "%Y-%m-%d %H:%M:%S", "%Y-%m-%dT%H:%M:%S.%Z", "%Y-%m-%d %H:%M:%S.%Z", "%m/%d/%Y"]),
        interval: int = typer.Option(None),
        limit: int = typer.Option(100),
        agg: str = typer.Option(None),
        order_by: str = typer.Option(None),
        use_strict_data_types: bool = typer.Option(False, '--use-strict-data-types'),
        csv: bool = typer.Option(False, '--csv'),
        table: bool = typer.Option(False, '--table'),
        tablefmt: str = typer.Option('simple'),
        format_output_date: bool = typer.Option(False, '--format-output-date'),
        timezone: str = typer.Option(None),
        output: Path = typer.Option(sys.stdout, exists=False, file_okay=True, dir_okay=False)):

    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client

    try:
        if not keys:
            keys = ",".join(rest_client.get_timeseries_keys_v1(entity_type=cli_context.target_entity.id.entity_type, entity_id=cli_context.target_entity.id.id))
            typer.echo(f"Retrieving all keys: {keys}")

        timeseries = rest_client.get_timeseries(
            entity_type=cli_context.target_entity.id.entity_type,
            entity_id=cli_context.target_entity.id.id,
            keys=keys,
            start_ts=int(start_ts.timestamp()*1e3),
            end_ts=int(end_ts.timestamp()*1e3),
            order_by=order_by,
            limit = limit,
            interval=interval,
            agg = agg,
            use_strict_data_types=use_strict_data_types)

        if csv or table:
            df: pd.DataFrame = pd.DataFrame()

            for key, records in  timeseries.items():
                series: pd.Series = pd.Series({r['ts']: r['value'] for r in records}, name=key)
                df = df.join(series, how='outer')

            if not timeseries:
                typer.echo('No timeseries found')
                return
            
            columns_diff = (set(keys.split(',')) - set(df.columns))
            if len(columns_diff) > 0:
                for new_col in columns_diff:
                    df[new_col] = np.nan

            df = df.reindex(columns=keys.split(','))

            df.index.name = 'ts'
            df.index = pd.to_datetime(df.index, unit='ms')

            if timezone:
                df.index = df.index.tz_localize('UTC').tz_convert(timezone)

            if not output.exists():
                output.parents[0].mkdir(parents=True, exist_ok=True)

            if csv:
                df.sort_index().to_csv(output)
            else:
                typer.echo(tabulate(df.sort_index(), headers=df.columns, tablefmt=tablefmt))
        else:
            typer.echo(timeseries)
    except Exception as e:
        typer.echo(e)
        raise typer.Exit(code=1)

@app.command()
def save(ctx: typer.Context, stdin: bool = typer.Option(False, '--stdin'), csv: Path = typer.Option(None), json: Path = typer.Option(None)):
    pass

@app.command()
def delete(ctx: typer.Context, entity_type: str, entity_id: str, keys: str):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        rest_client.delete_entity_attributes1(entity_id=EntityId(entity_type=entity_type,id=entity_id))
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

