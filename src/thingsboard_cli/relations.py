from xml.dom.minidom import Entity
from tb_rest_client.rest_client_ce import RestClientCE # type: ignore
from tb_rest_client.models.models_ce import EntityId, EntityRelation  # type: ignore
from tb_rest_client.rest import ApiException # type: ignore

import json

from typing import List, Optional
import typer

from thingsboard_cli.cli_context import CliContext, EntityType

app = typer.Typer()

@app.command()
def create(ctx: typer.Context, from_id: str, from_type: EntityType, relation_type:str, to_id: str, to_type: EntityType, from_entity_is_uuid: bool = typer.Option(False, "--from-entity-is-uuid"), to_entity_is_uuid: bool = typer.Option(False, "--to-entity-is-uuid")):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client

    try:

        if not from_entity_is_uuid:
            from_entity_id = cli_context.get_entity_id_by_name(name=from_id, type=from_type)
        else:
            from_entity_id = EntityId(entity_type=from_type, id=from_id)

        if not to_entity_is_uuid:
            to_entity_id = cli_context.get_entity_id_by_name(name=to_id, type=to_type)
        else:
            to_entity_id = EntityId(entity_type=to_type, id=to_id)

        entity_relation: EntityRelation = EntityRelation(type=relation_type, _from=from_entity_id, to=to_entity_id)

        rest_client.save_relation(body=entity_relation)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

@app.command()
def delete(ctx: typer.Context, from_id: str, from_type: EntityType, relation_type:str, to_id: str, to_type: EntityType, from_entity_is_uuid: bool = typer.Option(False, "--from-entity-is-uuid"), to_entity_is_uuid: bool = typer.Option(False, "--to-entity-is-uuid")):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        from_entity_id: EntityId = None
        if not from_entity_is_uuid:
            from_entity_id = cli_context.get_entity_id_by_name(name=from_id, type=from_type)
        else:
            from_entity_id = EntityId(entity_type=from_type, id=from_id)

        to_entity_id: EntityId = None
        if not to_entity_is_uuid:
            to_entity_id = cli_context.get_entity_id_by_name(name=to_id, type=to_type)
        else:
            to_entity_id = EntityId(entity_type=to_type, id=to_id)

        rest_client.delete_relation(from_id=from_entity_id.id, from_type=from_type, relation_type=relation_type, to_id=to_entity_id.id, to_type=to_type)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")
