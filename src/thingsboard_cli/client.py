import thingsboard_api_client

class Client(object):

    class Client:
        def __init__(self):
            self._api_client = None

        def init_client(self, host, port, username, password, debug):
            configuration = thingsboard_api_client.Configuration()
            configuration.host = "http://{}:{}".format(host,port)
            configuration.username = username
            configuration.password = password
            configuration.api_key_prefix['X-Authorization'] = "Bearer"
            configuration.verify_ssl = False
            configuration.debug = debug

            self._api_client = thingsboard_api_client.ThingsboardApiClient(configuration)
        
        def getClient(self):
            return self._api_client

    instance = None

    def __new__(cls): # __new__ always a classmethod
        if not Client.instance:
            Client.instance = Client.Client()
        return Client.instance

    def __getattr__(self, name):
        return getattr(self.instance, name)
        
    def __setattr__(self, name):
        return setattr(self.instance, name)