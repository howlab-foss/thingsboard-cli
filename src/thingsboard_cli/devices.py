from tb_rest_client.rest_client_ce import RestClientCE # type: ignore
from tb_rest_client.models.models_ce import Device, DeviceId  # type: ignore
from tb_rest_client.rest import ApiException # type: ignore

from thingsboard_cli.cli_context import CliContext
import thingsboard_cli.telemetry as telemetry
import thingsboard_cli.attributes as attributes
import thingsboard_cli.assign as assign

import json

from typing import Optional, List
import typer

app = typer.Typer()
app.add_typer(telemetry.app, name="telemetry")
app.add_typer(attributes.app, name="attributes")
app.add_typer(assign.app, name="assign")

ENTITY_COMMANDS = ('delete','get','attributes','assign')

@app.callback(invoke_without_command=True)
def main(ctx: typer.Context, uuid: str = typer.Option(None), name: str= typer.Option(None)):
    cli_context: CliContext = ctx.obj
    if uuid is not None:
        cli_context.target_entity = cli_context.rest_client.get_device_by_id(DeviceId('DEVICE', uuid))
    elif name is not None:
        cli_context.target_entity = cli_context.rest_client.get_tenant_device(name)
    elif ctx.invoked_subcommand in ENTITY_COMMANDS:
        raise typer.BadParameter("Must provide either UUID or name")

@app.command()
def list(ctx: typer.Context, page_size: int = typer.Option(100),
         page: int = typer.Option(0), type: str = typer.Option(None),
         text_search: str = typer.Option(None),
         sort_property: str = typer.Option(None), sort_order: str = typer.Option(None)):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        if cli_context.customer is not None:
            device_list = rest_client.get_customer_device_infos(
                customer_id=cli_context.customer.id,
                page=page, page_size=page_size, type=type,
                text_search=text_search, sort_property=sort_property,
                sort_order=sort_order)
        elif cli_context.is_tenant_admin():
            device_list = rest_client.get_tenant_device_infos(
                page=page, page_size=page_size, type=type,
                text_search=text_search, sort_property=sort_property,
                sort_order=sort_order)

        typer.echo(json.dumps(device_list.to_dict(), indent=4))
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")


@app.command()
def create(ctx: typer.Context, device_name: str, device_type: str, label: Optional[str] = typer.Option(None), public: bool= typer.Option(False)):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client

    device: Device = Device(name=device_name, type=device_type, label=label)

    if cli_context.customer is not None:
        device.customer_id = cli_context.customer.id

    try:
        typer.echo(device)
        device = rest_client.save_device(device)
        if public:
            device = rest_client.assign_device_to_public_customer(device.id)
        typer.echo(device)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

@app.command()
def delete(ctx: typer.Context, force: bool = typer.Option(False, "--force")):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client

    if not force:
        delete = typer.confirm("Are you sure you want to delete it?", abort=True)
    try:
        rest_client.delete_device(cli_context.target_entity.id)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

@app.command()
def get(ctx: typer.Context):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        typer.echo(cli_context.target_entity)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")
