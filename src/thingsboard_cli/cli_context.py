
from tb_rest_client.rest_client_ce import RestClientCE # type: ignore
from tb_rest_client.models.models_ce import EntityId, CustomerId, User, Asset, Device, EntityView, Customer # type: ignore
from tb_rest_client.rest import ApiException # type: ignore

from typing import Any
from enum import Enum

class EntityType(str, Enum):
    ASSET = "ASSET"
    DEVICE = "DEVICE"
    ENTITY_VIEW = "ENTITY_VIEW"
    CUSTOMER = "CUSTOMER"


class CliContext():

    rest_client: RestClientCE
    customer: CustomerId
    user: User
    target_entity: Any

    def __init__(self):
        self.rest_client = None
        self.customer = None
        self.user = None
        self.target_entity = None

    def is_customer_user(self):
        return self.user.authority == 'CUSTOMER_USER' if self.user is not None else False

    def is_tenant_admin(self):
        return self.user.authority == 'TENANT_ADMIN' if self.user is not None else False

    def get_entity_id_by_name(self, name: str, type: EntityType) -> EntityId:
        if type == EntityType.ASSET:
            asset: Asset = self.rest_client.get_tenant_asset(name)
            return asset.id
        elif type == EntityType.DEVICE:
            device: Device = self.rest_client.get_tenant_device(name)
            return device.id
        elif type == EntityType.ENTITY_VIEW:
            entity_view: EntityView = self.rest_client.get_tenant_entity_view(name)
            return entity_view.id
        elif type == EntityType.CUSTOMER:
            customer: Customer = self.rest_client.get_tenant_customer(name)
            return customer.id
        else:
            return None