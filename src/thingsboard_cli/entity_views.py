
from tb_rest_client.rest_client_ce import RestClientCE # type: ignore
from tb_rest_client.models.models_ce import EntityId, EntityView, EntityViewId, TelemetryEntityView, AttributesEntityView  # type: ignore
from tb_rest_client.rest import ApiException # type: ignore

from enum import Enum

import json
import yaml

from typing import Optional, List
import typer

import thingsboard_cli.telemetry as telemetry
import thingsboard_cli.assign as assign
from thingsboard_cli.cli_context import CliContext


class EntityType(str, Enum):
    ASSET = "ASSET"
    DEVICE = "DEVICE"    

app = typer.Typer()
app.add_typer(telemetry.app, name="telemetry")
app.add_typer(assign.app, name="assign")

ENTITY_COMMANDS = ('delete','telemetry','assign')

@app.callback(invoke_without_command=True)
def main(ctx: typer.Context, uuid: str = typer.Option(None), name: str= typer.Option(None)):
    cli_context: CliContext = ctx.obj
    try:
        if uuid is not None:
            cli_context.target_entity = cli_context.rest_client.get_entity_view_by_id(EntityViewId('ENTITY_VIEW', uuid))
        elif name is not None:
            cli_context.target_entity = cli_context.rest_client.get_tenant_entity_view(name)
        elif ctx.invoked_subcommand in ENTITY_COMMANDS:
            raise typer.BadParameter("Must provide either UUID or name")
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")
        typer.Exit(code=1)

@app.command()
def list(ctx: typer.Context, page_size: int = typer.Option(100),
         page: int = typer.Option(0), type: str = typer.Option(None),
         text_search: str = typer.Option(None),
         sort_property: str = typer.Option(None), sort_order: str = typer.Option(None)):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        if cli_context.customer is not None:
            asset_list = rest_client.get_customer_entity_view_infos(
                customer_id=cli_context.customer.id,
                page=page, page_size=page_size, type=type,
                text_search=text_search, sort_property=sort_property,
                sort_order=sort_order)
        elif cli_context.is_tenant_admin():
            asset_list = rest_client.get_tenant_entity_view_infos(
                page=page, page_size=page_size, type=type,
                text_search=text_search, sort_property=sort_property,
                sort_order=sort_order)

        asset_list = asset_list.to_dict()
        for asset in asset_list["data"]:
            asset['additional_info']= json.dumps(yaml.safe_load(asset['additional_info']))

        typer.echo(json.dumps(asset_list, indent=4))
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

@app.command()
def create(ctx: typer.Context, name: str, type: str,
           entity_type: EntityType,
           entity_id: str,
           additional_info: str = typer.Option(None),
           telemetry_timeseries: List[str]= typer.Option(None), 
           telemetry_cs_attributes: List[str]= typer.Option(None), 
           telemetry_sh_attributes: List[str]= typer.Option(None),
           telemetry_ss_attributes: List[str]= typer.Option(None)):

    cli_context: CliContext = ctx.obj    
    rest_client: RestClientCE = cli_context.rest_client

    entity_view_keys = TelemetryEntityView(
        timeseries=telemetry_timeseries,
        attributes=AttributesEntityView(cs=telemetry_cs_attributes, sh=telemetry_sh_attributes, ss=telemetry_ss_attributes))

    ev: EntityView = EntityView(
        name=name,
        type=type,
        entity_id=EntityId(entity_type=entity_type, id=entity_id),
        additional_info=additional_info,
        keys=entity_view_keys)

    try:
        ev = rest_client.save_entity_view(ev)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")        

@app.command()
def delete(ctx: typer.Context, force: bool = typer.Option(False, "--force")):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client

    if not force:
        delete = typer.confirm("Are you sure you want to delete it?", abort=True)

    try:
        rest_client.delete_entity_view(cli_context.target_entity.id)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")

@app.command()
def get(ctx: typer.Context):
    cli_context: CliContext = ctx.obj
    rest_client: RestClientCE = cli_context.rest_client
    try:
        typer.echo(cli_context.target_entity)
    except ApiException as e:
        typer.echo(f"{e.status}: {json.loads(e.body)['message']}")